package com.asky.companion_plugin

import android.Manifest
import android.app.Activity
import android.bluetooth.BluetoothAdapter
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import androidx.core.app.ActivityCompat

object AndrBluetoothUtil{

    var bluetoothAdapter: BluetoothAdapter = BluetoothAdapter.getDefaultAdapter()

    init {

    }

    //蓝牙是否已打开
    fun bleIsEnabled(): Boolean {
        return bluetoothAdapter != null && bluetoothAdapter.isEnabled
    }

    //该设备是否支持蓝牙
    fun isSupport(context: Context): Boolean {
        //return bluetoothAdapter != null
        return context.applicationContext.packageManager.hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)
    }

    //打开蓝牙（在华为手机上好像不好使）
    fun openBleSetting(activity: Activity,requestCode:Int) {
        var intent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
        if (ActivityCompat.checkSelfPermission(
                activity,
                Manifest.permission.BLUETOOTH_CONNECT
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            return
        }
        activity.startActivityForResult(intent, requestCode)
    }
}