package com.asky.companion_plugin

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.content.pm.PackageManager.NameNotFoundException
import android.net.Uri
import android.os.Build
import android.provider.Settings
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import io.flutter.embedding.engine.plugins.FlutterPlugin
import io.flutter.embedding.engine.plugins.activity.ActivityAware
import io.flutter.embedding.engine.plugins.activity.ActivityPluginBinding
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugin.common.MethodChannel.MethodCallHandler
import io.flutter.plugin.common.MethodChannel.Result


/** CompanionPlugin */
class CompanionPlugin : FlutterPlugin, MethodCallHandler, ActivityAware {

    private lateinit var channel: MethodChannel
    private lateinit var activity: Activity
    private lateinit var context: Context

    private var result: Result? = null

    private val REQUEST_BLUETOOTH_CODE = 1000
    private val REQUEST_BLUETOOTH_ADMIN_CODE = 1001
    private val REQUEST_BLUETOOTH_SCAN_CODE = 1002
    private val REQUEST_BLUETOOTH_CONNECT_CODE = 1003
    private val BLE_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1004

    private val REQUEST_ENABLE_BLE = 2000


    override fun onAttachedToEngine(flutterPluginBinding: FlutterPlugin.FlutterPluginBinding) {
        channel = MethodChannel(flutterPluginBinding.binaryMessenger, "companion_plugin")
        channel.setMethodCallHandler(this)
        context = flutterPluginBinding.applicationContext
    }

    override fun onMethodCall(call: MethodCall, result: Result) {
        if (call.method == "getPlatformVersion") {
            result.success("Android ${android.os.Build.VERSION.RELEASE}")

        } else if (call.method == "openURL") {
            val tempURLStr = call.argument<String>("url") ?: ""
            if (openURL(tempURLStr)) {
                result.success(true)
            } else {
                result.error("open fail", "", false)
            }

        } else if (call.method == "openSetting") {
            if (openSetting()) {
                result.success(true)
            } else {
                result.error("open fail", "", false)
            }

        } else if (call.method == "openWifi") {
            if (openWifi()) {
                result.success(true)
            } else {
                result.error("open fail", "", false)
            }

        } else if (call.method == "openBluetooth") {
            if (openBluetooth()) {
                result.success(true)
            } else {
                result.error("open fail", "", false)
            }

        } else if (call.method == "openAppSysSetting") {
            openAppSysSetting()
            result.success(null)

        } else if (call.method == "checkBleCanUse") {
            //检查蓝牙的状态
            checkBleCanUse(result)
        } else if (call.method == "getAppVersion") {
            //获取软件的版本号
            try {
                val pm = context.packageManager
                val packageInfo =
                    pm.getPackageInfo(context.packageName, PackageManager.GET_CONFIGURATIONS)
                result.success(packageInfo.versionName)
            } catch (e: Exception) {
                result.success("1.0.0")
            }
        } else if (call.method == "getAppName") {
            //获取软件的版本号
            try {
                val pm = context.packageManager
                result.success(pm.getApplicationLabel(context.applicationInfo).toString())
            } catch (e: Exception) {
                result.success("")
            }
        } else if (call.method == "goAppStore") {
            print("TTTTTTTTT   goAppStore")
            val appID = call.argument<String>("appID") ?: ""
            try {
                val packageManager = context.packageManager
                packageManager.getPackageInfo(appID, PackageManager.GET_ACTIVITIES)
                if (activity != null) {
                    val marketUrl = "market://details?id=$appID"
                    val intent = Intent(Intent.ACTION_VIEW)
                    intent.setData(Uri.parse(marketUrl))
                    activity.startActivity(intent)
                    result.success(true)
                } else {
                    result.success(false)
                }
            } catch (e: NameNotFoundException) {
                result.success(false)
            }

        } else {
            result.notImplemented()
        }
    }

    //打开网址
    fun openURL(url: String): Boolean {
        try {
            if (activity != null) {
                val intent = Intent(Intent.ACTION_VIEW)
                intent.addCategory(Intent.CATEGORY_BROWSABLE);
                intent.setData(Uri.parse(url))
                activity.startActivity(intent)
                return true
            } else {
                return false
            }
        } catch (e: Exception) {
            return false
        }
    }

    //打开设置的界面
    fun openSetting(): Boolean {
        if (activity != null) {
            val intent = Intent(Settings.ACTION_SETTINGS)
            activity.startActivity(intent, null)
            return true
        } else {
            return false
        }
    }

    //打开设置的WiFi界面
    fun openWifi(): Boolean {
        if (activity != null) {
            val intent = Intent(Settings.ACTION_WIFI_SETTINGS)
            activity.startActivity(intent, null)
            return true
        } else {
            return false
        }
    }

    //打开设置的Bluetooth界面
    fun openBluetooth(): Boolean {
        if (activity != null) {
            val intent = Intent(Settings.ACTION_BLUETOOTH_SETTINGS)
            activity.startActivity(intent, null)
            return true
        } else {
            return false
        }
    }

    //跳转应用消息，间接打开应用权限设置-效率高
    fun openAppSysSetting() {
        var intent = Intent()
        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        var uri = Uri.fromParts("package", context.packageName, null)
        intent.setData(uri)
        context.startActivity(intent)
    }

    //检查蓝牙是否可以正常使用 1:没有蓝牙权限(蓝牙、蓝牙管理、扫描、连接)   2：没有位置权限
    //3:蓝牙打开  4:蓝牙关闭  5：该设备不支持蓝牙  6:取消了返回结果
    fun checkBleCanUse(result: Result) {
        if (this.result != null && this.result != result) {
            this.result?.success(6)
            this.result = result
        } else {
            this.result = result
        }
        //1.检查设备是否支持蓝牙
        if (!AndrBluetoothUtil.isSupport(activity)) {
            //不支持
            result.success(5)
            this.result = null
            return
        }

        if (ContextCompat.checkSelfPermission(
                activity,
                Manifest.permission.BLUETOOTH
            )
            != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                activity, arrayOf<String>(Manifest.permission.BLUETOOTH),
                REQUEST_BLUETOOTH_CODE
            );
            return
        }

        if (ContextCompat.checkSelfPermission(
                activity,
                Manifest.permission.BLUETOOTH
            )
            != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                activity, arrayOf<String>(Manifest.permission.BLUETOOTH),
                REQUEST_BLUETOOTH_CODE
            );
            return
        }

        if (ContextCompat.checkSelfPermission(
                activity,
                Manifest.permission.BLUETOOTH_ADMIN
            )
            != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                activity, arrayOf<String>(Manifest.permission.BLUETOOTH_ADMIN),
                REQUEST_BLUETOOTH_ADMIN_CODE
            );
            return
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            if (ContextCompat.checkSelfPermission(
                    activity,
                    Manifest.permission.BLUETOOTH_SCAN
                )
                != PackageManager.PERMISSION_GRANTED
            ) {
                ActivityCompat.requestPermissions(
                    activity, arrayOf<String>(Manifest.permission.BLUETOOTH_SCAN),
                    REQUEST_BLUETOOTH_SCAN_CODE
                );
                return
            }

            if (ContextCompat.checkSelfPermission(
                    activity,
                    Manifest.permission.BLUETOOTH_CONNECT
                )
                != PackageManager.PERMISSION_GRANTED
            ) {
                ActivityCompat.requestPermissions(
                    activity, arrayOf<String>(Manifest.permission.BLUETOOTH_CONNECT),
                    REQUEST_BLUETOOTH_CONNECT_CODE
                );
                return
            }
        } else {
            //Android 12以下的手机需要这个权限，因为蓝牙可以获取到用户的位置
            if (ContextCompat.checkSelfPermission(
                    activity,
                    Manifest.permission.ACCESS_FINE_LOCATION
                )
                != PackageManager.PERMISSION_GRANTED
            ) {
                ActivityCompat.requestPermissions(
                    activity, arrayOf<String>(Manifest.permission.ACCESS_FINE_LOCATION),
                    BLE_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION
                );
                return
            }
        }

        if (AndrBluetoothUtil.bleIsEnabled()) {
            result.success(3)
        } else {
            result.success(4)
        }
        this.result = null
    }

    override fun onDetachedFromEngine(binding: FlutterPlugin.FlutterPluginBinding) {
        channel.setMethodCallHandler(null)
    }

    override fun onAttachedToActivity(activityPB: ActivityPluginBinding) {
        activity = activityPB.activity
        activityPB.addRequestPermissionsResultListener { requestCode, permissions, grantResults ->
            this.result?.let { rst ->
                //检查蓝牙是否可以正常使用 1:没有蓝牙权限(蓝牙、蓝牙管理、扫描、连接)   2：没有位置权限
                //3:蓝牙打开  4:蓝牙关闭  5：该设备不支持蓝牙  6:取消了返回结果
                if (REQUEST_BLUETOOTH_CODE == requestCode ||
                    REQUEST_BLUETOOTH_ADMIN_CODE == requestCode ||
                    REQUEST_BLUETOOTH_SCAN_CODE == requestCode ||
                    REQUEST_BLUETOOTH_CONNECT_CODE == requestCode
                ) {
                    if (grantResults.first() == PackageManager.PERMISSION_GRANTED) {
                        checkBleCanUse(rst)
                    } else {
                        rst.success(1)
                        this.result = null
                    }
                } else if (BLE_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION == requestCode) {
                    if (grantResults.first() == PackageManager.PERMISSION_GRANTED) {
                        checkBleCanUse(rst)
                    } else {
                        rst.success(2)
                        this.result = null
                    }
                }
            }

            true
        }

        activityPB.addActivityResultListener { requestCode, resultCode, intent ->
            if (REQUEST_ENABLE_BLE == requestCode) {

            }
            true
        }
    }

    override fun onDetachedFromActivityForConfigChanges() {
        TODO("Not yet implemented")
    }

    override fun onReattachedToActivityForConfigChanges(p0: ActivityPluginBinding) {
        TODO("Not yet implemented")
    }

    override fun onDetachedFromActivity() {
        TODO("Not yet implemented")
    }
}

