import 'dart:ffi';

import 'package:flutter_test/flutter_test.dart';
import 'package:companion_plugin/companion_plugin.dart';
import 'package:companion_plugin/companion_plugin_platform_interface.dart';
import 'package:companion_plugin/companion_plugin_method_channel.dart';
import 'package:plugin_platform_interface/plugin_platform_interface.dart';

class MockCompanionPluginPlatform
    with MockPlatformInterfaceMixin
    implements CompanionPluginPlatform {
  @override
  Future<String?> getPlatformVersion() => Future.value('42');

  @override
  Future<bool?> openBluetooth() {
    // TODO: implement openBluetooth
    throw UnimplementedError();
  }

  @override
  Future<bool?> openSetting() {
    // TODO: implement openSetting
    throw UnimplementedError();
  }

  @override
  Future<bool?> openURL(String url) {
    // TODO: implement openURL
    throw UnimplementedError();
  }

  @override
  Future<bool?> openWifi() {
    // TODO: implement openWifi
    throw UnimplementedError();
  }

  @override
  Future<Void?> scanPeripheral(dynamic serviceIDs) {
    // TODO: implement scanPeripheral
    throw UnimplementedError();
  }

  @override
  Future<Void?> stopScanPeripheral() {
    // TODO: implement stopScanPeripheral
    throw UnimplementedError();
  }

  @override
  Future<int?> checkBleCanUse() {
    // TODO: implement checkBleCanUse
    throw UnimplementedError();
  }

  @override
  Future<Void?> openAppSysSetting() {
    // TODO: implement openAppSysSetting
    throw UnimplementedError();
  }

  @override
  Future<Void?> connectPeripheral(peripheral) {
    // TODO: implement connectPeripheral
    throw UnimplementedError();
  }

  @override
  Future<Void?> cancelConnectPeripheral(peripheral) {
    // TODO: implement cancelConnectPeripheral
    throw UnimplementedError();
  }

  @override
  Future<String?> getAppVersion() {
    // TODO: implement getAppVersion
    throw UnimplementedError();
  }

  @override
  Future<String?> getAppName() {
    // TODO: implement getAppName
    throw UnimplementedError();
  }

  @override
  Future<bool?> goAppStore(String appID) {
    // TODO: implement goAppStore
    throw UnimplementedError();
  }

  @override
  Future<bool?> canUseLocation() {
    // TODO: implement canUseLocation
    throw UnimplementedError();
  }
}

void main() {
  final CompanionPluginPlatform initialPlatform =
      CompanionPluginPlatform.instance;

  test('$MethodChannelCompanionPlugin is the default instance', () {
    expect(initialPlatform, isInstanceOf<MethodChannelCompanionPlugin>());
  });

  test('getPlatformVersion', () async {
    CompanionPlugin companionPlugin = CompanionPlugin();
    MockCompanionPluginPlatform fakePlatform = MockCompanionPluginPlatform();
    CompanionPluginPlatform.instance = fakePlatform;

    expect(await companionPlugin.getPlatformVersion(), '42');
  });
}
