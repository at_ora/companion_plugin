//
//  MBluetoothUtil.swift
//  Runner
//
//  Created by magical on 2024/4/17.
//

import Foundation
import CoreBluetooth

class MBluetoothUtil: NSObject{
	static let shared = MBluetoothUtil()
	private override init() {}
	
	private let nslock = NSLock()
	
	var bleState:CBManagerState{
    	get{
    		return self.centralManager.state
    	}
    }
	
	var scanedPeripherals:[CBPeripheral] = []
	var currentConPeripheral:CBPeripheral?
	
	//蓝牙状态发生改变
	var bleStateChanged:((_ state:CBManagerState)->Void)?
	
	//蓝牙权限发生改变
	var blePermissionChanged:((_ authorizatio:CBManagerAuthorization)->Void)?
	
	//扫描到有那些外设
	var receivedPeripheral:((_ peripheral:CBPeripheral,_ advertisementData: [String : Any], _ rssi: NSNumber)->Void)?
	
	//连接外设成功
	var connectedSuccess:((_ peripheral:CBPeripheral)->Void)?
	
	//连接外设失败
	var connectedFail:((_ peripheral:CBPeripheral)->Void)?
	
	//取消了与外设的连接
	var disconnectBlock:(()->Void)?
	
	//收到了外设有哪些服务
	var receivedServices:((_ services:[CBService]?)->Void)?
	
	//收到了外设有哪些服务
	var receivedCharacteristics:((_ service: CBService)->Void)?
	
	//收到了外设发过来的数据
	var receivedCharacteristicData:((_ peripheral: CBPeripheral,_ characteristic: CBCharacteristic)->Void)?
	
	
	lazy var centralManager: CBCentralManager = {
    	let manager = CBCentralManager.init(delegate: self, queue: DispatchQueue.main,options:
    	    [CBCentralManagerOptionShowPowerAlertKey:false,
    	    CBCentralManagerOptionRestoreIdentifierKey:"com.asky.companion_app_ble_identifierKey"] //不弹出链接新设备的系统弹窗
    	)
    		return manager
    }()
	
	//检查蓝牙权限状态
	func checkBlePermissions() -> CBManagerAuthorization {
		return centralManager.authorization
	}
	
	//扫描周围的蓝牙(需要有权限及打开的情况下才能扫描)
	func scanPeripheral(serviceIDs:[String]?) -> Void {
		if centralManager.authorization == .notDetermined || centralManager.authorization == .denied  {
			return
		}
		if bleState != .poweredOn {
			return
		}
		var services:[CBUUID] = []
		for sID in serviceIDs ?? [] {
			services.append(CBUUID(string: sID))
		}
		//如果正在扫描，先暂停，然后再重新扫描
		self.stopScan()
		centralManager.scanForPeripherals(withServices: services)
	}
	
	//停止扫描
	func stopScan() -> Void {
		if centralManager.isScanning {
			centralManager.stopScan()
		}
	}

	//连接设备
	func connect(_ peripheral: CBPeripheral, options: [String : Any]? = nil){
		centralManager.connect(peripheral, options: options)
	}
	
	//取消连接
	func cancelPeripheralConnection(_ peripheral: CBPeripheral){
		centralManager.cancelPeripheralConnection(peripheral)
	}
	
	//清除已扫描到的设备
	func clearScanedPeripherals() -> Void {
		nslock.lock()
		scanedPeripherals.removeAll()
		nslock.unlock()
	}
	
	//发现服务
	func discoverService(peripheral:CBPeripheral,serviceIDs:[String]?) -> Void {
		self.currentConPeripheral = peripheral
		self.currentConPeripheral?.delegate = self
		var services:[CBUUID] = []
		for sID in serviceIDs ?? [] {
			services.append(CBUUID(string: sID))
		}
		self.currentConPeripheral?.discoverServices(services)
	}
	
	//发现特征
	func discoverCharacteristics(characteristicIDs:[String]?,service:CBService) -> Void {
		var characteristics:[CBUUID] = []
		for cID in characteristicIDs ?? [] {
			characteristics.append(CBUUID(string: cID))
		}
		self.currentConPeripheral?.discoverCharacteristics(characteristics, for: service)
	}
	
	//发送数据
	func sendMsg(data:Data,characteristic:CBCharacteristic)-> Void {
		self.currentConPeripheral?.writeValue(data, for: characteristic, type: CBCharacteristicWriteType.withResponse)
	}
	
}

extension MBluetoothUtil:CBPeripheralDelegate {
	
	func peripheralDidUpdateName(_ peripheral: CBPeripheral) {
		debugPrint("peripheralDidUpdateName")
	}
	
	func peripheral(_ peripheral: CBPeripheral, didModifyServices invalidatedServices: [CBService]) {
		debugPrint("didModifyServices")
	}
	
	func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: (any Error)?) {
		debugPrint("didDiscoverServices")
		print(peripheral.services ?? "")
		self.receivedServices?(peripheral.services)
		
	}
	
	func peripheral(_ peripheral: CBPeripheral, didDiscoverIncludedServicesFor service: CBService, error: (any Error)?) {
		debugPrint("didDiscoverIncludedServicesFor")
	}
	
	func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: (any Error)?) {
		debugPrint("didDiscoverCharacteristicsFor")
		self.receivedCharacteristics?(service)
	}
	
	func peripheral(_ peripheral: CBPeripheral, didDiscoverDescriptorsFor characteristic: CBCharacteristic, error: (any Error)?) {
		debugPrint("didDiscoverDescriptorsFor")
	}
	
	
	func peripheral(_ peripheral: CBPeripheral, didOpen channel: CBL2CAPChannel?, error: (any Error)?) {
		debugPrint("didOpen")
	}
	
	
	func peripheral(_ peripheral: CBPeripheral, didWriteValueFor characteristic: CBCharacteristic, error: (any Error)?) {
		debugPrint("didWriteValueFor")
	}
	
	
	func peripheral(_ peripheral: CBPeripheral, didReadRSSI RSSI: NSNumber, error: (any Error)?) {
		debugPrint("didReadRSSI")
	}
	
	func peripheral(_ peripheral: CBPeripheral, didWriteValueFor descriptor: CBDescriptor, error: (any Error)?) {
		debugPrint("didWriteValueFor")
	}
	
	func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: (any Error)?) {
		debugPrint("didUpdateValueFor")
		self.receivedCharacteristicData?(peripheral,characteristic)
	}
	
	
	func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor descriptor: CBDescriptor, error: (any Error)?) {
		debugPrint("didUpdateValueFor")
	}
	

	func peripheralIsReady(toSendWriteWithoutResponse peripheral: CBPeripheral) {
		debugPrint("toSendWriteWithoutResponse")
	}
	
	func peripheral(_ peripheral: CBPeripheral, didUpdateNotificationStateFor characteristic: CBCharacteristic, error: (any Error)?) {
		debugPrint("didUpdateNotificationStateFor")
	}
	
}

extension MBluetoothUtil:CBCentralManagerDelegate{
	
	//蓝牙状态发生了改变
	func centralManagerDidUpdateState(_ central: CBCentralManager) {
		debugPrint("centralManagerDidUpdateState")
		bleStateChanged?(bleState)
	}
	
	//扫描到了外设
	func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
        debugPrint("didDiscover")
        debugPrint(peripheral.name ?? "***")
		nslock.lock()
		scanedPeripherals.append(peripheral)
		receivedPeripheral?(peripheral,advertisementData,RSSI)
		nslock.unlock()
	}
	
	//连接成功
	func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
		debugPrint("didConnect")
		connectedSuccess?(peripheral)
	}
	
	//连接失败
	func centralManager(_ central: CBCentralManager, didFailToConnect peripheral: CBPeripheral, error: (any Error)?) {
		debugPrint("didFailToConnect")
		if let e = error {
			debugPrint(e)
		}
		connectedFail?(peripheral)
	}
	
	//断开连接
	func centralManager(_ central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, timestamp: CFAbsoluteTime, isReconnecting: Bool, error: (any Error)?) {
		debugPrint("didDisconnectPeripheral isReconnecting")
		if let e = error {
			debugPrint(e)
		}
		disconnectBlock?();
	}
	
	//外设备断开了连接
	func centralManager(_ central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: (any Error)?) {
		debugPrint("didDisconnectPeripheral")
		if let e = error {
			debugPrint(e)
		}
		disconnectBlock?();
	}
	
	//授权状态发生了改变
	func centralManager(_ central: CBCentralManager, didUpdateANCSAuthorizationFor peripheral: CBPeripheral) {
		debugPrint("didUpdateANCSAuthorizationFor")
		blePermissionChanged?(central.authorization)
	}
	
	//事件发生
	func centralManager(_ central: CBCentralManager, connectionEventDidOccur event: CBConnectionEvent, for peripheral: CBPeripheral) {
		debugPrint("connectionEventDidOccur")
	}
	
	func centralManager(_ central: CBCentralManager, willRestoreState dict: [String : Any]) {
		debugPrint("willRestoreState")
	}
	
}
