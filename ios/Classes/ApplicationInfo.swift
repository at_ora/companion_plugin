//
//  ApplicationInfo.swift
//  MagicalTool
//
//  Created by magical on 2019/2/27.
//  Copyright © magical. All rights reserved.
//

import UIKit

/// 获取应用的信息
class ApplicationInfo: NSObject {

    /// 获取app的版本号
    /// - Returns: 返回当前app的版本号
    static func getVersion() -> String {
        
        guard let infoDic = Bundle.main.infoDictionary else {
            return "1.0"
        }
        return infoDic["CFBundleShortVersionString"] as! String
    }
    
    /// 获取app的编译版本号
    /// - Returns: 返回build号
    static func getBuildVersion() -> String {
        
        guard let infoDic = Bundle.main.infoDictionary else {
            return "1"
        }
        return infoDic["CFBundleVersion"] as! String
    }
    
    /// 获取app的BundleID
    /// - Returns: 返回BundleID
    static func getBundleID() -> String {
        
        guard let infoDic = Bundle.main.infoDictionary else {
            return ""
        }
        return infoDic["CFBundleIdentifier"] as! String
    }
    
    /// 获取app的名字
    /// - Returns: 返回的名称
    static func getAppName() -> String {
        
        guard let infoDic = Bundle.main.infoDictionary else {
            return ""
        }
        if let displayName = infoDic["CFBundleDisplayName"] { //如果设置了 display name ,那APP的名字就是这个
            return displayName as! String
        }
        return infoDic["CFBundleName"] as! String
    }
}
