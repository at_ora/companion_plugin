enum BleState {
  //没有位置权限，应该蓝牙可以获取到位置信息所以iPhone、
  // Android 12以下的设备需要位置权限
  notLocationPermission,
  notBlePermission,//没有蓝牙权限，被用户拒绝了蓝牙权限
  on, //蓝牙已打开
  off,//蓝牙关闭
}