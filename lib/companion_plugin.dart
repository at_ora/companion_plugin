import 'dart:ffi';

import 'common.dart';
import 'companion_plugin_platform_interface.dart';

class CompanionPlugin {
  Future<String?> getPlatformVersion() {
    return CompanionPluginPlatform.instance.getPlatformVersion();
  }

  //打开网址
  Future<bool?> openURL(String url) {
    return CompanionPluginPlatform.instance.openURL(url);
  }

  //打开设置的界面
  Future<bool?> openSetting() {
    return CompanionPluginPlatform.instance.openSetting();
  }

  //打开设置的WiFi界面
  Future<bool?> openWifi() {
    return CompanionPluginPlatform.instance.openWifi();
  }

  //打开设置的Bluetooth界面
  Future<bool?> openBluetooth() {
    return CompanionPluginPlatform.instance.openBluetooth();
  }

  //跳转应用消息，间接打开应用权限设置(only android)
  Future<Void?> openAppSysSetting() {
    return CompanionPluginPlatform.instance.openAppSysSetting();
  }

  //检查蓝牙是否可以正常使用 1:没有蓝牙权限(蓝牙、蓝牙管理、扫描、连接)   2：没有位置权限
  //3:蓝牙打开  4:蓝牙关闭  5：该设备不支持蓝牙
  Future<int?> checkBleCanUse() {
    return CompanionPluginPlatform.instance.checkBleCanUse();
  }

  //扫描周围蓝牙设备
  Future<Void?> scanPeripheral(dynamic serviceIDs) {
    return CompanionPluginPlatform.instance.scanPeripheral(serviceIDs);
  }

  //停止扫描周围蓝牙设备
  Future<Void?> stopScanPeripheral() {
    return CompanionPluginPlatform.instance.stopScanPeripheral();
  }

  //连接外设
  Future<Void?> connectPeripheral(dynamic peripheral) {
    return CompanionPluginPlatform.instance.connectPeripheral(peripheral);
  }

  //取消与外接设备的连接
  Future<Void?> cancelConnectPeripheral(dynamic peripheral) {
    return CompanionPluginPlatform.instance.cancelConnectPeripheral(peripheral);
  }

  //获取应用的版本号
  Future<String?> getAppVersion() async {
    return CompanionPluginPlatform.instance.getAppVersion();
  }

  //获取应用的名称
  Future<String?> getAppName() async {
    return CompanionPluginPlatform.instance.getAppName();
  }

  //跳到在AppStore山歌界面
  Future<bool?> goAppStore(String appID) async {
    return CompanionPluginPlatform.instance.goAppStore(appID);
  }

  //检查是否已获得位置权限
  Future<bool?> canUseLocation() async {
    return CompanionPluginPlatform.instance.canUseLocation();
  }
}
