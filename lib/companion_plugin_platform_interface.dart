import 'dart:ffi';

import 'package:plugin_platform_interface/plugin_platform_interface.dart';

import 'common.dart';
import 'companion_plugin_method_channel.dart';

abstract class CompanionPluginPlatform extends PlatformInterface {
  /// Constructs a CompanionPluginPlatform.
  CompanionPluginPlatform() : super(token: _token);

  static final Object _token = Object();

  static CompanionPluginPlatform _instance = MethodChannelCompanionPlugin();

  /// The default instance of [CompanionPluginPlatform] to use.
  ///
  /// Defaults to [MethodChannelCompanionPlugin].
  static CompanionPluginPlatform get instance => _instance;

  /// Platform-specific implementations should set this with their own
  /// platform-specific class that extends [CompanionPluginPlatform] when
  /// they register themselves.
  static set instance(CompanionPluginPlatform instance) {
    PlatformInterface.verifyToken(instance, _token);
    _instance = instance;
  }

  Future<String?> getPlatformVersion() {
    throw UnimplementedError('platformVersion() has not been implemented.');
  }

  Future<bool?> openURL(String url) {
    throw UnimplementedError('openWifi() has not been implemented.');
  }

  Future<bool?> openSetting() {
    throw UnimplementedError('openSetting() has not been implemented.');
  }

  Future<bool?> openWifi() {
    throw UnimplementedError('openWifi() has not been implemented.');
  }

  Future<bool?> openBluetooth() {
    throw UnimplementedError('openBluetooth() has not been implemented.');
  }

  Future<Void?> openAppSysSetting() {
    throw UnimplementedError('openBluetooth() has not been implemented.');
  }

  Future<int?> checkBleCanUse() {
    throw UnimplementedError('openBluetooth() has not been implemented.');
  }

  Future<Void?> scanPeripheral(dynamic serviceIDs) {
    throw UnimplementedError('scanPeripheral() has not been implemented.');
  }

  Future<Void?> stopScanPeripheral() {
    throw UnimplementedError('stopScanPeripheral() has not been implemented.');
  }

  //连接外设
  Future<Void?> connectPeripheral(dynamic peripheral) {
    throw UnimplementedError('connectPeripheral() has not been implemented.');
  }

  //取消连接外设
  Future<Void?> cancelConnectPeripheral(dynamic peripheral) {
    throw UnimplementedError(
        'cancelConnectPeripheral() has not been implemented.');
  }

  //获取应用的版本号
  Future<String?> getAppVersion() {
    throw UnimplementedError('getAppVersion() has not been implemented.');
  }

  //获取应用的名称
  Future<String?> getAppName() async {
    throw UnimplementedError('getAppName() has not been implemented.');
  }

  //跳到在AppStore山歌界面
  Future<bool?> goAppStore(String appID) async {
    throw UnimplementedError('goAppStore has not been implemented.');
  }

  Future<bool?> canUseLocation() async {
    throw UnimplementedError('canUseLocation has not been implemented.');
  }
}
