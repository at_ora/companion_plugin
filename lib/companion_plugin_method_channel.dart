import 'dart:ffi';

import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';

import 'common.dart';
import 'companion_plugin_platform_interface.dart';

/// An implementation of [CompanionPluginPlatform] that uses method channels.
class MethodChannelCompanionPlugin extends CompanionPluginPlatform {
  /// The method channel used to interact with the native platform.
  @visibleForTesting
  final methodChannel = const MethodChannel('companion_plugin');

  @override
  Future<String?> getPlatformVersion() async {
    final version =
        await methodChannel.invokeMethod<String>('getPlatformVersion');
    return version;
  }

  @override
  Future<bool?> openURL(String url) async {
    return await methodChannel.invokeMethod<bool>('openURL', {"url": url});
  }

  @override
  Future<bool?> openSetting() async {
    return await methodChannel.invokeMethod<bool>('openSetting');
  }

  @override
  Future<bool?> openWifi() async {
    return await methodChannel.invokeMethod<bool>('openWifi');
  }

  @override
  Future<bool?> openBluetooth() async {
    return await methodChannel.invokeMethod<bool>('openBluetooth');
  }

  Future<Void?> openAppSysSetting() async {
    return await methodChannel.invokeMethod<Void>('openAppSysSetting');
  }

  @override
  Future<int?> checkBleCanUse() async {
    return await methodChannel.invokeMethod<int>('checkBleCanUse');
  }

  @override
  Future<Void?> scanPeripheral(dynamic serviceIDs) async {
    return await methodChannel.invokeMethod<Void>('scanPeripheral', serviceIDs);
  }

  @override
  Future<Void?> stopScanPeripheral() async {
    return await methodChannel.invokeMethod<Void>('stopScanPeripheral');
  }

  //连接外设
  @override
  Future<Void?> connectPeripheral(dynamic peripheral) async {
    return await methodChannel.invokeMethod<Void>(
        'connectPeripheral', peripheral);
  }

  //取消连接外设
  @override
  Future<Void?> cancelConnectPeripheral(dynamic peripheral) async {
    return await methodChannel.invokeMethod<Void>(
        'cancelConnectPeripheral', peripheral);
  }

  //获取应用的版本号
  @override
  Future<String?> getAppVersion() async {
    return await methodChannel.invokeMethod<String>('getAppVersion');
  }

  //获取应用的名称
  Future<String?> getAppName() async {
    return await methodChannel.invokeMethod<String>('getAppName');
  }

  //跳到在AppStore山歌界面
  @override
  Future<bool?> goAppStore(String appID) async {
    return await methodChannel
        .invokeMethod<bool>('goAppStore', {"appID": appID});
  }

  //检查是否已获得位置权限
  @override
  Future<bool?> canUseLocation() async {
    return await methodChannel.invokeMethod<bool>('canUseLocation');
  }
}
