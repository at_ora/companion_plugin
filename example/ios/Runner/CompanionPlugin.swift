import Flutter
import CoreBluetooth
import UIKit
import CoreLocation
import StoreKit

public class CompanionPlugin: NSObject, FlutterPlugin {
	
	private var result:FlutterResult?
	private var isCanUseLoationRequest = false
	
	private lazy var locationManager: CLLocationManager = {
		let lm = CLLocationManager()
		lm.delegate = self
		return lm
	}()
	
	private var registrar: FlutterPluginRegistrar?
	private lazy var sendChanel: FlutterMethodChannel? = {
		if let reg = registrar {
			let c = FlutterMethodChannel(name: "com.asky.companion/fromAndroidOrIOS", binaryMessenger: reg.messenger())
			return c
		}else{
			return nil
		}
	}()
	
	override init() {
		super.init()
		print("PPPPPPPPPPPPPP")
		MBluetoothUtil.shared.bleStateChanged = {
			//检查蓝牙是否可以正常使用 1:没有蓝牙权限(蓝牙、蓝牙管理、扫描、连接)   2：没有位置权限
			//3:蓝牙打开  4:蓝牙关闭  5：该设备不支持蓝牙  6:取消了返回结果
			[weak self] state in
			switch state {
			case .unsupported:
				self?.result?(5)
			case .poweredOn:
				self?.checkBleCanUse()
			case .poweredOff:
				self?.result?(4)
			default:
				self?.result?(1)
			}
		}
		
		MBluetoothUtil.shared.connectedSuccess = {
			peripheral in
			print("连接\(peripheral.name ?? "")成功了")
			MBluetoothUtil.shared.discoverService(peripheral: peripheral, serviceIDs: nil)
		}
		
		MBluetoothUtil.shared.connectedFail = {
			peripheral in
			print("连接\(peripheral.name ?? "")失败了")
		}
		
		MBluetoothUtil.shared.disconnectBlock = {
			debugPrint("连接断开了")
			let vc  = UIViewController()
			UIApplication.shared.delegate?.window??.rootViewController?.present(vc, animated: true)
			
		}
		
		
		
		MBluetoothUtil.shared.receivedServices = {
			services in
			for item in services ?? []{
				print("发现了服务")
				print(item.uuid.uuidString)
			}
		}
		
		MBluetoothUtil.shared.receivedPeripheral = {
			[weak self] peripheral,advertisementData,rssi in
			let name = (advertisementData["kCBAdvDataLocalName"] as? String ?? peripheral.name) ?? ""
			self?.sendChanel?.invokeMethod("receiveScanResult", arguments: ["name":name,"uuID":peripheral.identifier.uuidString])
		}
	}
	
	public static func register(with registrar: FlutterPluginRegistrar) {
		let channel = FlutterMethodChannel(name: "companion_plugin", binaryMessenger: registrar.messenger())
		let instance = CompanionPlugin()
		instance.registrar = registrar
		registrar.addMethodCallDelegate(instance, channel: channel)
	}
	
	
	public func handle(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
		switch call.method {
		case "getPlatformVersion":
			result("iOS " + UIDevice.current.systemVersion)
			
		case "openURL":
			guard let dic = call.arguments as? Dictionary<String, String> else {
				result(false)
				return
			}
			let urlStr = (dic["url"]) ?? ""
			openURL(url:urlStr,result:result)
			
		case "openSetting":
			openSetting(result:result)
			
		case "openWifi":
			openWifi(result:result)
			
		case "openBluetooth":
			openBluetooth(result:result)
			
		case "checkBleCanUse":
			if self.result != nil {
				self.result?(6)
			}
			self.isCanUseLoationRequest = false
			self.result = result
			checkBleCanUse()
			
		case "scanPeripheral":
			let serviceIDs = call.arguments as? Array<String>
			MBluetoothUtil.shared.scanPeripheral(serviceIDs: serviceIDs)
			self.sendChanel?.invokeMethod("receiveScanResult", arguments: ["mag":"我是你大爷"])
			
			
		case "stopScanPeripheral":
			MBluetoothUtil.shared.stopScan()
			
		case "goAppStore":
			guard let dic = call.arguments as? Dictionary<String, String> else {
				result(false)
				return
			}
			let appID = (dic["appID"]) ?? ""
			let vc = SKStoreProductViewController()
			vc.loadProduct(withParameters: [SKStoreProductParameterITunesItemIdentifier:appID]) { success, err in
			}
			UIViewController.current?.present(vc, animated: true)
			
		case "canUseLocation":
			if self.result != nil {
				self.result?(6)
			}
			self.isCanUseLoationRequest = true
			self.result = result
			canUseLocation()
			
		default:
			result(FlutterMethodNotImplemented)
		}
	}
	
	
	//打开url
	func openURL(url: String,result: @escaping FlutterResult){
		guard let url = URL(string: url) else {
			result(false)
			return
		}
		if (UIApplication.shared.canOpenURL(url)) {
			UIApplication.shared.open(url) { success in
				result(success)
			}
		}else{
			result(false)
		}
	}
	
	//跳转到设置的界面
	func openSetting(result: @escaping FlutterResult){
		openURL(url: UIApplication.openSettingsURLString, result: result)
	}
	
	
	//跳转到WiFi打开关闭的界面
	func openWifi(result: @escaping FlutterResult){
		openURL(url: "App-prefs:WIFI", result: result)
	}
	
	//跳转到Bluetooth打开关闭的界面
	func openBluetooth(result: @escaping FlutterResult){
		openURL(url: "App-Prefs:Bluetooth", result: result)
	}
	
	//检查蓝牙是否可以正常使用 1:没有蓝牙权限(蓝牙、蓝牙管理、扫描、连接)   2：没有位置权限
	//3:蓝牙打开  4:蓝牙关闭  5：该设备不支持蓝牙  6:取消了返回结果
	func checkBleCanUse (){
		guard let rst = self.result else { return }
		if MBluetoothUtil.shared.bleState == .unsupported {
			rst(5)
			return
		}
		
		//蓝牙权限
		let authorization = MBluetoothUtil.shared.checkBlePermissions()
		if authorization == .notDetermined {
			debugPrint("用户第一次使用到蓝牙")
			return
		}else if authorization == .denied{
			debugPrint("用户已经拒绝了使用蓝牙")
			rst(1)
			return
		}
		
		//位置权限
		switch CLLocationManager.authorizationStatus() {
		case .notDetermined:
			print("权限未确定，请求权限")
			locationManager.requestWhenInUseAuthorization()
			return
		case .restricted, .denied:
			//print("权限受限或被拒绝")
			// 引导用户到设置中去开启权限
			rst(2)
			return
		case .authorizedAlways, .authorizedWhenInUse:
			//print("已授权位置权限")
			break
		@unknown default:
			rst(2)
			return
		}
		
		if MBluetoothUtil.shared.bleState == .poweredOn {
			rst(3)
		}else{
			//poweredOff resetting
			rst(4)
		}
	}
	
	
	func canUseLocation(){
		guard let rst = self.result else { return }
		//位置权限
		switch CLLocationManager.authorizationStatus() {
		case .notDetermined:
			print("权限未确定，请求权限")
			locationManager.requestWhenInUseAuthorization()
			return
		case .restricted, .denied:
			//print("权限受限或被拒绝")
			// 引导用户到设置中去开启权限
			rst(false)
			return
		case .authorizedAlways, .authorizedWhenInUse:
			//print("已授权位置权限")
			rst(true)
			break
		@unknown default:
			rst(false)
		}
		self.isCanUseLoationRequest = false
	}
	
}


extension CompanionPlugin:CLLocationManagerDelegate{
	//位置权限发生改变
	public func locationManagerDidChangeAuthorization(_ manager: CLLocationManager) {
		print("_____________________________")
		print(CLLocationManager.authorizationStatus().rawValue)
		switch CLLocationManager.authorizationStatus() {
		case .notDetermined:
			break
		case .restricted, .denied:
			//print("权限受限或被拒绝")
			// 引导用户到设置中去开启权限
			if(isCanUseLoationRequest){
				self.result?(false)
				self.isCanUseLoationRequest = false
			}else{
				self.result?(2)
			}
		case .authorizedAlways, .authorizedWhenInUse:
			print("已授权位置权限")
			if(isCanUseLoationRequest){
				self.result?(true)
				self.isCanUseLoationRequest = false
			}else{
				self.checkBleCanUse()
			}
		default:
			if(isCanUseLoationRequest){
				self.result?(false)
				self.isCanUseLoationRequest = false
			}else{
				self.result?(2)
			}
		}
	}
}

extension UIViewController {
	static var current:UIViewController? {
		let delegate  = UIApplication.shared.delegate as? AppDelegate
		var current = delegate?.window?.rootViewController
		
		while (current?.presentedViewController != nil)  {
			current = current?.presentedViewController
		}
		
		if let tabbar = current as? UITabBarController , tabbar.selectedViewController != nil {
			current = tabbar.selectedViewController
		}
		
		while let navi = current as? UINavigationController , navi.topViewController != nil  {
			current = navi.topViewController
		}
		return current
	}
}
