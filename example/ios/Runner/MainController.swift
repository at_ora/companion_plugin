//
//  MainController.swift
//  Runner
//
//  Created by magical on 2024/4/17.
//

import UIKit

class MainController: UIViewController {
	
	
	@IBOutlet weak var tableView: UITableView!
	
    override func viewDidLoad() {
        super.viewDidLoad()
		
//		MBluetoothUtil.shared.receivedPeripheral = {[weak self] (peripheral, advertisementData, rssi) in
////			print("*************s*******************")
////			print(peripheral.name)
////			print(peripheral.identifier.uuidString)
////			print(advertisementData)
////			print(rssi)
////			print("*************e*******************")
//			self?.tableView.reloadData()
//		}
//		
//		
//		MBluetoothUtil.shared.connectedSuccess = {
//			peripheral in
//			MBluetoothUtil.shared.discoverService(peripheral: peripheral, serviceIDs: nil)
//		}
		
		self.tableView.dataSource = self
		self.tableView.delegate = self
		self.tableView.register(UINib.init(nibName: "PCell", bundle: nil), forCellReuseIdentifier: "PCell")
		self.tableView.rowHeight = 40

    }
    
	@IBAction func checkPAction(_ sender: Any) {
		print("XXXXX")
		
		print("PPPPPPPPPPPPPPPPPPPPP")
	}
	
	@IBAction func openBleAction(_ sender: Any) {
	}
	
	@IBAction func closeBleAction(_ sender: Any) {
	}
	
	@IBAction func startScanAction(_ sender: Any) {
		MBluetoothUtil.shared.scanPeripheral(serviceIDs:["0000b81d-0000-1000-8000-00805f9b34fb"])
	}
	
	@IBAction func stopScanAction(_ sender: Any) {
		MBluetoothUtil.shared.stopScan()
	}
	
	@IBAction func sendMsgAction(_ sender: Any) {
	}
	
}

extension MainController:UITableViewDataSource,UITableViewDelegate{
	
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return MBluetoothUtil.shared.scanedPeripherals.count
	}
	
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "PCell", for: indexPath) as! PCell
		let m = MBluetoothUtil.shared.scanedPeripherals[indexPath.row]
		cell.msgLab.text = m.name ?? m.identifier.uuidString
		return cell
	}
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		let m = MBluetoothUtil.shared.scanedPeripherals[indexPath.row]
		MBluetoothUtil.shared.connect(m)
		tableView.deselectRow(at: indexPath, animated: false)
	}
	
}
